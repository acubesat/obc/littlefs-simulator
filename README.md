## LittleFS in-orbit simulator
A command-line simulator for LittleFS with configurable file size and
operations, used to gather useful information about wear leveling and
filesystem overhead.

Usage:
```bash
node --max-old-space-size=6000 demo.js
```

### Simulation Results
```
Key (date):                 file_visit_count read_blocks prog_blocks read_total prog_total erase_total wear_level
Packet Storage 2020-11-06:  182007           2092        2092        300180587  59698715   365546      4.5%
Payload Storage 2020-11-06: 3240             8651        8651        202952358  1292581    17304       0.1%
```

### littlefs-js A javascript wrapper for the little filesystem

Source: https://github.com/littlefs-project/littlefs-js

This project puts together two things that should probably never
go together:  
embedded system filesystems, and web-side javascript.

The result is a fully functional javascript API for littlefs, complete
with simulated block devices. This was all built using emscripten, a
backend for the LLVM that can compile C to javascript. There's no smoke
and mirrors here, this is actually running littlefs in your browser.

**littlefs** - https://github.com/geky/littlefs  
**emscripten** - https://github.com/kripken/emscripten  

So you want to see littlefs running in your browser? Just follow this link to
the demo!  
http://littlefs.geky.net/demo.html

You can find the full implementation of the demo in [demo.html](demo.html).
