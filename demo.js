var fs = require('fs');
var crypto = require("crypto");
eval(fs.readFileSync('lfs_js_bd.js')+'');
eval(fs.readFileSync('lfs_js.js')+'');
eval(fs.readFileSync('lfs.js')+'');

//
// === PACKET STORAGE MEMORY ===
//

// PASS_COUNT = 4
// TOTAL_PASSES = 762

// BLOCK_SIZE = 1048576
// BLOCK_COUNT = 8192

// STATIC_FILE_COUNT   = 3
// STATIC_FILE_SIZE    = 512
// DYNAMIC_FILE_COUNT  = PASS_COUNT * 237
// DYNAMIC_FILE_SIZE   = 1018
// ERASE_CYCLES        = 1000

//
// === PAYLOAD MEMORY ===
//

PASS_COUNT = 1
TOTAL_PASSES = 3

BLOCK_SIZE = 1048576
BLOCK_COUNT = 8192 * 2

STATIC_FILE_COUNT   = 3
STATIC_FILE_SIZE    = 512
DYNAMIC_FILE_COUNT  = 1080
DYNAMIC_FILE_SIZE   = 3.72 * 1024 * 1024
ERASE_CYCLES        = 1000



function throttle(func, wait, options) {
    var context, args, result;
    var timeout = null;
    var previous = 0;
    if (!options) options = {};
    var later = function() {
      previous = options.leading === false ? 0 : Date.now();
      timeout = null;
      result = func.apply(context, args);
      if (!timeout) context = args = null;
    };
    return function() {
      var now = Date.now();
      if (!previous && options.leading === false) previous = now;
      var remaining = wait - (now - previous);
      context = this;
      args = arguments;
      if (remaining <= 0 || remaining > wait) {
        if (timeout) {
          clearTimeout(timeout);
          timeout = null;
        }
        previous = now;
        result = func.apply(context, args);
        if (!timeout) context = args = null;
      } else if (!timeout && options.trailing !== false) {
        timeout = setTimeout(later, remaining);
      }
      return result;
    };
  };

var fileVisitCount = 0;

function redrawin() {
    var reads = 0;
    var progs = 0;
    var readstotal = 0;
    var progstotal = 0;

    for (var i = 0; i < BLOCK_COUNT; i++) {
        // opdata.setCell(1+i, 1+0, readops[i]?1:0);
        // opdata.setCell(1+i, 1+1, progops[i]?1:0);
        // opdata.setCell(1+i, 1+2, eraseops[i]?1:0);
        readstotal += readops[i] || 0
        progstotal += progops[i] || 0
        reads += readops[i]?1:0
        progs += progops[i]?1:0
    }

    var e = 0
    for (var i = 0; i < erases.length; i++) {
        e += erases[i] || 0
    }

    var w = e / (BLOCK_COUNT*ERASE_CYCLES)
    var wear = (w*100).toFixed(1) + "%"

    console.log(fileVisitCount + " " + reads + " " + progs + " " + readstotal + " " + progstotal + " " + e + " " + wear)
}

redraw = throttle(redrawin, 200);


function onread(block) {
    readops[block] = (readops[block] || 0) + 1
    redraw()
}

function onprog(block) {
    if (erases[block] > ERASE_CYCLES) {
        redraw()
        return false
    }

    progops[block] = (progops[block] || 0) + 1
    redraw()
}

function onerase(block) {
    if (erases[block] > ERASE_CYCLES) {
        redraw()
        return false
    }

    erases[block] = (erases[block] || 0) + 1
    eraseops[block] = (eraseops[block] || 0) + 1
    redraw()
}

console.log("Welcome to demo.js")

reset()

for (i = 1; i < TOTAL_PASSES + PASS_COUNT; i += PASS_COUNT) {
    step();
    console.log("================ Simulated pass #" + i + "/" + TOTAL_PASSES)
}


function finit() {
    // format and mount
    var err = lfs.format()
    if (err) {
        return err
    }

    var err = lfs.mount()
    if (err) {
        return err
    }

    // create static files
    for (var i = 0; i < STATIC_FILE_COUNT; i++) {
        var f = lfs.open('static' + i, ['wronly', 'creat'])
        if (f < 0) {
            return err
        }

        for (var j = 0; j < STATIC_FILE_SIZE; j += 16) {
            var err = f.write('0123456789abcdef')
                if (err < 0) {
                    return err
            }
        }

        var err = f.close()
        if (err) {
            return err
        }

        console.log("Added file " + i)
    }

    // unmount
    var err = lfs.unmount()
    if (err) {
        return err
    }

    return 0
}

function fstep() {
    // mount
    var err = lfs.mount()
    if (err) {
        return err
    }

    // rebuild dynamic files
    for (var i = 0; i < DYNAMIC_FILE_COUNT; i++) {
        fileVisitCount += 1
        var f = lfs.open('dynamic' + i, ['wronly', 'creat'])
        if (f < 0) {
            return err;
        }

        var remaining = DYNAMIC_FILE_SIZE;
        while (remaining > 0) {
            var bytes = Math.min(remaining, 256 * 1024);

            var err = f.write(randomString(bytes));
            if (err < 0) {
                return err
            }

            remaining -= bytes;
        }

        var err = f.close()
        if (err < 0) {
            return err
        }
    }

    // unmount
    var err = lfs.unmount()
    if (err) {
        return err
    }

    return 0;
}

function apply() {
    simulating = false

    var gridlines = BLOCK_COUNT;
    var multiplier = 1
    while (gridlines > 64) {
        gridlines /= 2
        multiplier *= 2
    }
    var ticks = []
    for (var i = 0; i <= gridlines; i++) {
        ticks.push(i*multiplier)
    }

    // setup block devices and lfs
    bd = new BD(1, 1, BLOCK_SIZE, BLOCK_SIZE*BLOCK_COUNT)
    bd.onread = onread
    bd.onprog = onprog
    bd.onerase = onerase

    lfs = new LFS(bd)

    // rejigger charts
    // if (erasedata.getNumberOfRows()-1 > BLOCK_COUNT) {
    //     erasedata.removeRows(1+BLOCK_COUNT,
    //         erasedata.getNumberOfRows()-1-BLOCK_COUNT)
    // }
    // for (var i = erasedata.getNumberOfRows(); i <= BLOCK_COUNT; i++) {
    //     erasedata.addRow([i, 0])
    // }
    // eraseoptions.hAxis.ticks = ticks
    // eraseoptions.vAxis.viewWindow.max = ERASE_CYCLES

    // if (useddata.getNumberOfRows()-1 > BLOCK_COUNT) {
    //     useddata.removeRows(1+BLOCK_COUNT,
    //         useddata.getNumberOfRows()-1-BLOCK_COUNT)
    // }
    // for (var i = useddata.getNumberOfRows(); i <= BLOCK_COUNT; i++) {
    //     useddata.addRow([i, 0])
    // }
    // usedoptions.hAxis.ticks = ticks

    // if (opdata.getNumberOfRows()-1 > BLOCK_COUNT) {
    //     opdata.removeRows(1+BLOCK_COUNT,
    //         opdata.getNumberOfRows()-1-BLOCK_COUNT)
    // }
    // for (var i = opdata.getNumberOfRows(); i <= BLOCK_COUNT; i++) {
    //     opdata.addRow([i, 0, 0, 0])
    // }
    // opoptions.hAxis.ticks = ticks

    // grab user scripts
    // finit = Function(initeditor.getValue())
    // fstep = Function(stepeditor.getValue())

    // setup filesystem and storage
    var err = finit()
    boots += 1
    if (err) {
        console.log('error:', err)
    }

    // trigger first draw
}

function reset() {
    simulating = false

    // set things to zero
    erases = []
    reads = 0
    progs = 0
    readops = []
    progops = []
    eraseops = []
    boots = 0

    // pass on to apply
    apply()

    // trigger redraw
    redraw()
}

function step() {
    simulating = false

    var err = fstep()
    boots += 1
    if (err < 0) {
        console.log('error:', err)
    }
}

var simulating = false
function simulate() {
    if (simulating) {
        simulating = false
        return
    }

    function sim() {
        if (!simulating) {
            return
        }

        var err = fstep()
        boots += 1
        if (err < 0) {
            console.log('error:', err)
            simulating = false
        }

    }

    simulating = true
}

function randomString(length) {
    var result           = '';
    var characters       = 'AB9ft5';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(i%charactersLength);
    }
    return result;
 }
